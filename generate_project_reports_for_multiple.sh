#!/bin/bash

project_reports_parent_dir=$1 # directory containing the individual project report directories
scripts_loc=$(dirname "$0") # path to dir containing this bash script (hence all scripts)
r_script_to_create_reports=${scripts_loc}/create_project_report.R

for report_dir in $(ls -d $project_reports_parent_dir/*_report_dir)
do
    project_id=$(basename $report_dir '_report_dir')
    data_file=${project_id}_analysis_data.tsv
    if [[ -f $project_reports_parent_dir/${project_id}_report_dir/${project_id}_report.html ]]; then
        echo "Skipping $project_id. Report already exists ($project_reports_parent_dir/${project_id}_report_dir/${project_id}_report.html)"
    else
        echo "Generating report for $project_id"
        Rscript \
            --no-save --no-restore --no-site-file --no-init-file \
            $r_script_to_create_reports \
            -d $report_dir -f $data_file -p $project_id
    fi
done

mkdir -p ${project_reports_parent_dir}/all_project_reports
cp ${project_reports_parent_dir}/*_report_dir/*.html ${project_reports_parent_dir}/all_project_reports
zip -q ${project_reports_parent_dir}/all_project_reports.zip \
    ${project_reports_parent_dir}/all_project_reports/*.html

echo -e "\n\nAll project report files saved to ${project_reports_parent_dir}/all_project_reports"
echo -e "\nReport files have also been zipped: ${project_reports_parent_dir}/all_project_reports.zip"