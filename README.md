# SEQAFRICA Project Reports Generator

This workflow uses a combination of bash and R scripts to generate Rmarkdown reports for analysed SEQAFRICA WGS datasets.

### Usage
`bash generate_project_reports_for_multiple.sh <PATH_TO_PARENT_DIR_OF_ALL_PROJECT_DIRS>`

This calls the R script to generate Rmarkdown html reports for each project directory in <PATH_TO_PARENT_DIR_OF_ALL_REPORT_DIRS>. All generated html reports are zipped into a final file called 'all_project_reports.zip'

